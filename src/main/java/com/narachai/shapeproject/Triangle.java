/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.narachai.shapeproject;

/**
 *
 * @author ASUS
 */
public class Triangle {
    private double h;
    private  double b;
    public static final double p = 0.5;
    public Triangle(double b,double h){
        this.b = b;
        this.h = h;
    }
    public double triangleArea(){
        return p*b*h;
    }
    public double getH(){
        return h;
    }
    public double getB(){
        return b;
    }
    public void setH(double h){
        if(h<=0) {
            System.out.println("Error: Radius must more than zero!!!!");
            return;
        }
        this.h = h;
    }
    public void setB(double b){
        if(b<=0) {
            System.out.println("Error: Radius must more than zero!!!!");
            return;
        }
        this.b = b;
    }
}
