/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.narachai.shapeproject;

/**
 *
 * @author ASUS
 */
public class Rectangle {
    private double h;
    private double w;
    public Rectangle(double h,double w){
        this.h = h;
        this.w = w;
    }
    public double rectangleArea(){
        if(w==h){
            System.out.println("Error:This is not a rectangle!!!!"); 
        }
        return h*w;
    }
    public double getH(){
        return h;
    }
    public double getW(){
        return w;
    }
    public void setH(double h){
        if(h<=0) {
            System.out.println("Error: Radius must more than zero!!!!");
            return;
        }
        this.h = h;
    }
    public void setW(double w){
        if(w<=0) {
            System.out.println("Error: Radius must more than zero!!!!");
            return;
        }
        this.w = w;
    }
}
