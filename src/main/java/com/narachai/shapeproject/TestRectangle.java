/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.narachai.shapeproject;

/**
 *
 * @author ASUS
 */
public class TestRectangle {
    public static void main(String[] args) {
        Rectangle rectangle1 = new Rectangle(3,4);
        System.out.println("Area of rectangle1 (H = "+ rectangle1.getH() +" W = "+ rectangle1.getW()+")is "+rectangle1.rectangleArea());
        rectangle1.setH(2);
        System.out.println("Area of rectangle1 (H = "+ rectangle1.getH() +" W = "+ rectangle1.getW()+")is "+rectangle1.rectangleArea());
        rectangle1.setW(2);
        System.out.println("Area of rectangle1 (H = "+ rectangle1.getH() +" W = "+ rectangle1.getW()+")is "+rectangle1.rectangleArea());
        rectangle1.setW(1);
        System.out.println("Area of rectangle1 (H = "+ rectangle1.getH() +" W = "+ rectangle1.getW()+")is "+rectangle1.rectangleArea());
        rectangle1.setW(0);
        System.out.println("Area of rectangle1 (H = "+ rectangle1.getH() +" W = "+ rectangle1.getW()+")is "+rectangle1.rectangleArea());
    }
}
